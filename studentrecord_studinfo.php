<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HEDCen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include_once('header.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                MENU
                <small>Admissions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Registrar</a></li>
                <li class="active">Admissions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="col-lg-12 container-fluid">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Student Information Form</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-warning alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5> NO PERMIT TO ENROLL FOUND FOR THIS STUDENT </h5>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Student No.</label>
                                        <div class="col-sm-8">
                                            <div class="btn btn-flat bg-olive">Generate Student No.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Years in School</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleInputFile" class="col-sm-4 control-label">
                                            <img class="profile-user-img img-responsive img-polaroid pull-right" src="dist/img/user8-128x128.jpg">
                                        </label>
                                        <br>
                                        <div class="col-sm-8">
                                            <input type="file" id="exampleInputFile">
                                        </div>
                                        <br><br>
                                        <div class="col-sm-8">
                                            <div class="btn btn-flat bg-olive btn-xs">Upload Image</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row container-fluid">
                                <div class="nav-tabs-custom ">
                                    <ul class="nav nav-tabs pull-left">
                                        <li class="active"><a href="#personal-data" data-toggle="tab">Personal Data</a></li>
                                        <li class=""><a href="#family-background" data-toggle="tab">Family Background</a></li>
                                        <li class=""><a href="#school-history" data-toggle="tab">School History</a></li>
                                        <li class=""><a href="#health-history" data-toggle="tab">Health History</a></li>
                                        <li class=""><a href="#requirements" data-toggle="tab">Requirements</a></li>
                                        <li class=""><a href="#permit" data-toggle="tab">Permit to Enroll</a></li>
                                    </ul>
                                    <div class="tab-content no-padding">

                                        <div class="chart tab-pane active container-fluid" id="personal-data" style="position: relative; height: 100%;">
                                            <br>
                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Last Name<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">First Name<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Mother's Maiden Name<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Mother's Maiden Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Middle Initial<span class="small text-warning">(optional)</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Middle Initial">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Nickname<span class="text-warning small">(optional)</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nickname">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Gender<span class="text-danger">*</span></label>
                                                            <br>
                                                                <input type="radio"  id="exampleInputEmail1"> Male
                                                                <input type="radio" id="exampleInputEmail19"> Female
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Birth Date<span class="text-danger">*</span></label>
                                                            <input type="date" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Age<span class="small text-danger">*</span></label>
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="years old">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <hr>
                                                <div class=""><h4><b>Current Address</b></h4></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Unit/House No.<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Unit/House No.">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Street<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Street">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Barangay / Subdivision<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Barangay/Subdivision">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3"></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">District/Municipality<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="District/Municipality">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">City / Province<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="City / Province">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Zip Code<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Zip Code">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3"></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label>Citizenship</label>
                                                            <select class="form-control select2"">
                                                                <option selected="selected">Filipino</option>
                                                                <option>Australian</option>
                                                                <option>American</option>
                                                                <option>German</option>
                                                                <option>Japanese</option>
                                                                <option>Korean</option>
                                                                <option>Chinese</option>
                                                            </select>
                                                        </div>
                                                    </div><!-- /.form-group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- phone mask -->
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label>Home Phone No.:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-earphone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div><!-- /.form group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- phone mask -->
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label>Mobile Phone No.:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div><!-- /.form group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label>Email Address:</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-envelope"></i>
                                                                </div>
                                                                <input type="email" class="form-control" >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div><!-- /.form group -->
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1"><span class="text-danger"></span></label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- phone mask -->
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label></label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-earphone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div><!-- /.form group -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- phone mask -->
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label></label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div><!-- /.form group -->
                                                </div>
                                                <div class="col-lg-3">

                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <hr>
                                                <div class=""><h4><b>Person to Contact in Case of Emergency</b></h4></div>
                                                    <div class="col-lg-9">
                                                        <form class="form-horizontal">
                                                                <div class="form-group">
                                                                    <label for="inputEmail3" class="col-sm-2 control-label">Parent / Legal Guardian<span class="text-danger">*</span></label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" id="inputEmail3" placeholder="Parent / Legal Guardian">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="inputPassword3" class="col-sm-2 control-label">Address<span class="text-danger">*</span></label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" class="form-control" id="inputPassword3" placeholder="Address">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="inputPassword3" class="col-sm-2 control-label">Email Address<span class="text-danger">*</span></label>
                                                                    <div class="col-sm-10">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">
                                                                                <i class="glyphicon glyphicon-envelope"></i>
                                                                            </div>
                                                                            <input type="email" class="form-control" placeholder="Email Address" >
                                                                        </div><!-- /.input group -->
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="inputPassword3" class="col-sm-2 control-label">Contact No.<span class="text-danger">*</span></label>
                                                                    <div class="col-sm-10">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon">
                                                                                <i class="glyphicon glyphicon-phone"></i>
                                                                            </div>
                                                                            <input type="text" class="form-control" placeholder="Phone / Mobile" >
                                                                        </div><!-- /.input group -->
                                                                    </div>
                                                                </div>
                                                        </form>
                                                    </div>
                                                    <div class="row col-lg-3">

                                                    </div>
                                                </div>

                                            <div class="row container-fluid">
                                                <hr>
                                                <div class=""><h4><b>Other Information</b></h4></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Place of Birth</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Place of Birth">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">No. Of Siblings</label>
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="No. of Siblings">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Birth Order(1st, 2nd, etc)</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Birth Order">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="radio"  id="exampleInputEmail1"> Biological Child
                                                            <br>
                                                            <input type="radio" id="exampleInputEmail19"> Adopted Child
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Language/Dialects Spoken at Home</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Language/Dialect">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Talents/Special Skills</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Hobbies/Interests</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <hr>
                                                <div class=""><h4><b>For Foreign Students Only</b></h4></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">ACR No.</label>
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Date of Issue:</label>
                                                            <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Place of Issue:</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="exampleInputEmail1">Passport No.:</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="submit" class="form-control btn btn-flat bg-olive" id="exampleInputEmail1" value="Add">
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="chart tab-pane container-fluid" id="family-background" style="position: relative; height: 100%;">
                                            <br>
                                            <div class="row container-fluid">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">
                                                    <div class="pull-left"><b>Father</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="pull-left"><b>Mother (Maiden)</b></div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Last Name<span class="text-danger">*</span></b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>First Name<span class="text-danger">*</span></b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Age</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-6">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Age">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-6">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Age">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Birth Date</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Place of Birth</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Place of Birth">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Place of Birth">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Marital Status</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Single</option>
                                                            <option>Divorced</option>
                                                            <option>Married</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Single</option>
                                                            <option>Divorced</option>
                                                            <option>Married</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="checkbox" > same as the student permanent address
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                                <input type="checkbox" > same as the student permanent address
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Home / House No.</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Home / House No.">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Home / House No.">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Street</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Street">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Street">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Brngy. / Subdivision</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Brngy. / Subdivision">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Brngy. / Subdivision">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>District Municipality</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="District Municipality">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="District Municipality">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>City / Province</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="City / Province">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="City / Province">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Home Ownership</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="radio" > Rent
                                                            <input type="radio" > Own
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="radio" > Rent
                                                            <input type="radio" > Own
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Home Tel. No. / Mobile No.</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Phone / Mobile" >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Phone / Mobile" >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Email Address</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-envelope"></i>
                                                                </div>
                                                                <input type="email" class="form-control" placeholder="Email Adress" >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-envelope"></i>
                                                                </div>
                                                                <input type="email" class="form-control" placeholder="Email Address" >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Religion</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Religion">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Religion">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Citizenship / Nationality</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Citizenship / Nationality">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Citizenship / Nationality">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Language Spoken</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Language Spoken">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Language Spoken">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Highest Educational Attainment</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Grade School</option>
                                                            <option>High School</option>
                                                            <option>College</option>
                                                            <option>Tech. / Vocational</option>
                                                            <option>Masters Degree</option>
                                                            <option>Doctors Degree</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Grade School</option>
                                                            <option>High School</option>
                                                            <option>College</option>
                                                            <option>Tech. / Vocational</option>
                                                            <option>Masters Degree</option>
                                                            <option>Doctors Degree</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>School Graduated From</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="School Graduated From">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="School Graduated From">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <p></p>
                                            <p></p>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Occupation / Position Held</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Occupation / Position Held">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Occupation / Position Held">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Employer / Company</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Employer / Company">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Employer / Company">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="checkbox" > OFW
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="checkbox" > OFW
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Business Address</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Business Address">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Business Address">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Business Tel. No.</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Business Tel. No." >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="glyphicon glyphicon-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="Business Tel. No." >
                                                            </div><!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Annual Income</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Annua Income">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Annual Income">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Total Household Income</b></div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">P 50,000 and below</option>
                                                            <option>P 50,0001 and up to P 75,000</option>
                                                            <option>P 75,0001 and up to P 100,000</option>
                                                            <option>P 100,0001 and up to P 150,000</option>
                                                            <option>P 150,0001 and above</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">

                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <p></p>
                                                    <p><b>Family Members</b></p>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Brothers / Sisters</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Birth Date</b></div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="pull-left"><b>Age</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Grade / Year Level Occupation</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>School / Employer</b></div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <p></p>
                                                    <p><b>Other family/household members living at home</b></p>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Name</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Relationship</b></div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="pull-left"><b>Age</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>Grade / Year Level Occupation</b></div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="pull-left"><b>School / Employer</b></div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="number" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <p></p>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <button type="submit" class="btn btn-flat bg-olive">Add</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="chart tab-pane container-fluid" id="school-history" style="position: relative; height: 100%;">
                                            <br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3"><b>School Attended</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3"><b>Gr./Yr. Level</b></div>
                                                <div class="col-lg-3"><b>Name of School</b></div>
                                                <div class="col-lg-3"><b>School Address</b></div>
                                                <div class="col-lg-3"><b>Inclusive Years (From - To)</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3"><b>Honors and Awards</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>Honors/Awards Most Recently Received</b>
                                                </div>
                                                <div class="col-lg-2">
                                                    <b>Academic Year</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>Has the child taken any special / advance courses / programs?</b>
                                                </div>
                                                <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <input type="radio" > Yes
                                                                <input type="radio" > No
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>If yes, please indicate course title and date taken.</b>
                                                </div>
                                                <div class="col-lg-2">
                                                    <b>Year</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>Has	the	child	taken	any summer classes?</b>
                                                </div>
                                                <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <input type="radio" > Yes
                                                                <input type="radio" > No
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>If yes, please indicate course title and date taken.</b>
                                                </div>
                                                <div class="col-lg-2">
                                                    <b>Year</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>Has	the	child	failed in any subject?</b>
                                                </div>
                                                <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <input type="radio" > Yes
                                                                <input type="radio" > No
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>If yes, please indicate subject/s failed and year taken</b>
                                                </div>
                                                <div class="col-lg-2">
                                                    <b>Year</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>Has	the	child	repeated any grade/level?</b>
                                                </div>
                                                <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <div class="col-xs-12">
                                                                <input type="radio" > Yes
                                                                <input type="radio" > No
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <b>If yes, state level repeated.</b>
                                                </div>
                                                <div class="col-lg-2">
                                                    <b>Year</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">+ add new row</button>
                                                    <button type="submit" class="btn btn-xs btn-flat bg-green-gradient">- remove last row</button>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-5"><b>For transferees:</b> Please indicate the schools and reasons for transfer</div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1">From</div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 text-right">To</div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            (previous school and Gr./Yr. Level)
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 text-right"></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            (present school and Gr./Yr. Level)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1">
                                                    <b>Reason</b>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1">From</div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 text-right">To</div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            (previous school and Gr./Yr. Level)
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 text-right"></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            (present school and Gr./Yr. Level)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-1">
                                                    <b>Reason</b>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <br><br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-9">
                                                    <b>Has the applicant been subjected to any disciplinary action in school? If yes, please describe and write the sanction.</b>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <button type="submit" class="btn btn-md btn-flat bg-olive">Add</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="chart tab-pane container-fluid" id="health-history" style="position: relative; height: 100%;">

                                            <br>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2"><b>Height (in meters)</b></div>
                                                <div class="col-lg-2"><b>Weight (in kg.)</b></div>
                                                <div class="col-lg-2"><b>Eyesight</b></div>
                                                <div class="col-lg-2"><b>Hearing</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4"><b>Allergies</b></div>
                                                <div class="col-lg-4"><b>Immunizations</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2"><b>Speech Difficulties</b></div>
                                                <div class="col-lg-2"><b>Childhood Diseases</b></div>
                                                <div class="col-lg-4"><b>Other Medical Condition</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2"><b>Name of Health Insurance</b></div>
                                                <div class="col-lg-2"><b>Family Doctor</b></div>
                                                <div class="col-lg-2"><b>Hospital/Clinic Tel. No.</b></div>
                                                <div class="col-lg-2"><b>Blood Type</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-6"><b>Special needs which should be taken into consideration.</b></div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <button type="submit" class="btn btn-md btn-flat bg-olive">Add</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="chart tab-pane container-fluid" id="requirements" style="position: relative; height: 100%;">
                                            <br>
                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <select class="form-control select2"">
                                                        <option selected="selected">2014-2015</option>
                                                        <option>2015-2016</option>
                                                        <option>2016-2017</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                        </div>

                                        <div class="chart tab-pane container-fluid" id="permit" style="position: relative; height: 100%;">
                                            <br>
                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <select class="form-control select2"">
                                                    <option selected="selected">2014-2015</option>
                                                    <option>2015-2016</option>
                                                    <option>2016-2017</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <b>Previous Grade/Year Level</b>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Select Student Level</option>
                                                            <option>First Time Student</option>
                                                            <option>Kinder 1</option>
                                                            <option>Kinder 2</option>
                                                            <option>Grade 1</option>
                                                            <option>Grade 2</option>
                                                            <option>Grade 3</option>
                                                            <option>Grade 4</option>
                                                            <option>Grade 5</option>
                                                            <option>Grade 6</option>
                                                            <option>Grade 7</option>
                                                            <option>Grade 8</option>
                                                            <option>Grade 9</option>
                                                            <option>Grade 10</option>
                                                            <option>Grade 11</option>
                                                            <option>Grade 12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <b>General Average</b>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <b>Present Year Level</b>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <select class="form-control select2"">
                                                            <option selected="selected">Select Student Level</option>
                                                            <option>First Time Student</option>
                                                            <option>Kinder 1</option>
                                                            <option>Kinder 2</option>
                                                            <option>Grade 1</option>
                                                            <option>Grade 2</option>
                                                            <option>Grade 3</option>
                                                            <option>Grade 4</option>
                                                            <option>Grade 5</option>
                                                            <option>Grade 6</option>
                                                            <option>Grade 7</option>
                                                            <option>Grade 8</option>
                                                            <option>Grade 9</option>
                                                            <option>Grade 10</option>
                                                            <option>Grade 11</option>
                                                            <option>Grade 12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-2">
                                                    <b></b>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <input type="checkbox" class=""> Mark as Approved
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row container-fluid">
                                                <div class="col-lg-3">
                                                    <button type="submit" class="btn btn-md btn-flat bg-olive">Save Permit</button>
                                                </div>
                                            </div>

                                            <br>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-flat bg-green">Update All</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->

            </div>

        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->


    <?php include_once('footer.php'); ?>

</body>
</html>

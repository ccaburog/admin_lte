<header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>H</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>HEDC</b>en</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu pull-left">
            <ul class="nav navbar-nav">

                <li class="dropdown messages-menu active">
                    <a href="index.php">
                        HOME
                    </a>
                </li>


                <li class="dropdown messages-menu">
                    <a href="admissions_applicant_information.php">
                        REGISTRAR
                    </a>
                </li>
<!---->
<!--                <li class="dropdown messages-menu">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
<!--                        GRADING-->
<!--                    </a>-->
<!--                </li>-->
<!---->
<!---->
<!--                <li class="dropdown messages-menu">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
<!--                        CASHIER-->
<!--                    </a>-->
<!--                </li>-->
<!---->
<!---->
<!--                <li class="dropdown messages-menu">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
<!--                        PAYROLL-->
<!--                    </a>-->
<!--                </li>-->



                <!-- Control Sidebar Toggle Button -->
                <li class="pull-right">
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/hedcen.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Holistic Education</p>
                <p>Development Center</p>

<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">

            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
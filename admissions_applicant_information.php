<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HEDCen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once('header.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                MENU
                <small>Inquiry</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Registrar</a></li>
                <li class="active">Inquiry</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-left">
                            <li class="active"><a href="#inquiry" data-toggle="tab">Inquiry</a></li>
                            <li><a href="#registration" data-toggle="tab">Registration</a></li>
                            <li><a href="#enrollment" data-toggle="tab">Enrollment</a></li>

                        </ul>

                        <div class="container-fluid">
                        <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="inquiry" style="position: relative; height: 100%;">
                        <div class="row">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-aqua">
                                    <span class="info-box-icon"><i class="fa fa-user-plus"></i><a href="admissions_add_student.php"></a></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Add New Student</span>
                                        <span class="info-box-number">
                                            <a href="admissions_add_student.php">Go</a>
                                        </span>
                                        <div class="progress">
                                            <div class="progress-bar" style="width: 100%"></div>
                                        </div>
                                    </div><!-- /.info-box-content -->
                                </div><!-- /.info-box -->
                            </div><!-- /.col -->


                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-green">
                                    <span class="info-box-icon"><i class="fa fa-user-plus"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Add Returning Student</span>
                                        <span class="info-box-number">
                                            <a href="admissions_add_return_student.php">Go</a>
                                        </span>
                                        <div class="progress">
                                            <div class="progress-bar" style="width: 100%"></div>
                                        </div>

                                    </div><!-- /.info-box-content -->
                                </div><!-- /.info-box -->
                            </div><!-- /.col -->


                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-orange-active">
                                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Generate Interviews schedule</span>
                                        <span class="info-box-number">
                                            <a href="admissions_generate_interview.php">Go</a>
                                        </span>
                                        <div class="progress">
                                            <div class="progress-bar" style="width: 100%"></div>
                                        </div>

                                    </div><!-- /.info-box-content -->
                                </div><!-- /.info-box -->
                            </div><!-- /.col -->

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-yellow-active">
                                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Generate Exam Shedule</span>
                                        <span class="info-box-number">
                                            <a href="admissions_generate_schedule_exam.php">Go</a>
                                        </span>
                                        <div class="progress">
                                            <div class="progress-bar" style="width: 100%"></div>
                                        </div>

                                    </div><!-- /.info-box-content -->
                                </div><!-- /.info-box -->
                            </div><!-- /.col -->



                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Search Area</h3>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form">
                                        <div class="box-body">

                                            <div class="form-group">
                                                <label for="applicantNumber">Applicant Number</label>
                                                <input type="email" class="form-control" id="applicantNumber" placeholder="Enter applicant number">
                                            </div>

                                            <div class="form-group">
                                                <label for="firstName">First Name</label>
                                                <input type="text" class="form-control" id="firstName" placeholder="First Name">
                                            </div>

                                            <div class="form-group">
                                                <label for="lastName">Last Name</label>
                                                <input type="text" class="form-control" id="lastName" placeholder="Last Name">
                                            </div>

                                            <div class="checkbox">
                                                <label>Gender</label>
                                                <label>
                                                    <input type="checkbox"> Male
                                                </label>
                                                <label>
                                                    <input type="checkbox"> Female
                                                </label>
                                            </div>

                                        </div><!-- /.box-body -->

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Search base on year and level</h3>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                    <form role="form">
                                        <div class="box-body">

                                            <div class="form-group">
                                                <label for="applicantNumber">Select School Year</label>
                                                <select class="form-control" name="" id="">
                                                    <option value="">2014-2015</option>
                                                    <option value="">2015-2016</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="applicantNumber">Select Level</label>
                                                <select class="form-control" name="" id="">
                                                    <option value="">Select Student Level</option>
                                                    <option value="17">Toddler</option>
                                                    <option value="12">Nursery</option>
                                                    <option value="14">Kinder</option>
                                                    <option value="16">Preparatory</option>
                                                    <option value="1">Grade 1</option>
                                                    <option value="2">Grade 2</option>
                                                    <option value="3">Grade 3</option>
                                                    <option value="4">Grade 4</option>
                                                    <option value="5">Grade 5</option>
                                                    <option value="6">Grade 6</option>
                                                    <option value="8">Grade 7</option>
                                                    <option value="9">Grade 8</option>
                                                    <option value="10">Grade 9</option>
                                                    <option value="11">Grade 10</option>
                                                </select>
                                            </div>

                                        </div><!-- /.box-body -->

                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>

                            </div>


                        </div>



                        <!-- row for the table-->
                        <div class="row container-fluid">


                        <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Student List</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length" id="example1_length">
                                    <label>Show <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div id="example1_filter" class="dataTables_filter">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row"><div class="col-sm-12">
                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 169px;">Applicant No.</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 215px;">Last Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 197px;">First Name</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 145px;">M.I</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Status</th>
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!--<tr role="row" class="odd">
                            <td class="sorting_1">Gecko</td>
                            <td>Firefox 1.0</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.7</td>
                            <td>A</td>
                            <td>A</td>
                        </tr>-->

                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">VERDAN</td>
                            <td class="divTblListTD">MARIA CARYL FAYE</td>
                            <td class="divTblListTD">J. </td>
                            <td class="divTblListTD">NEW</td>
                            <td>

                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">DEL CASTILLO</td>
                            <td class="divTblListTD">JUAN ALFONSO</td>
                            <td class="divTblListTD">Z.</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">ALCANTARA</td>
                            <td class="divTblListTD">JULLIAN IRAH</td>
                            <td class="divTblListTD">M.</td>
                            <td class="divTblListTD">NEW</td>
                            <td>

                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">RENTUTAR</td>
                            <td class="divTblListTD">MATTHEW ANGELO</td>
                            <td class="divTblListTD">T</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">534</td>
                            <td class="divTblListTD">FLORITA</td>
                            <td class="divTblListTD">NICOLE ANGELA</td>
                            <td class="divTblListTD">I.</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">LUCINARIO</td>
                            <td class="divTblListTD">ALEKSEI YOHANN</td>
                            <td class="divTblListTD">T</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">PASIA</td>
                            <td class="divTblListTD">EISEN CASSANDRA</td>
                            <td class="divTblListTD">M</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">SINGH</td>
                            <td class="divTblListTD">GURPAWANDEEP</td>
                            <td class="divTblListTD"></td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">530</td>
                            <td class="divTblListTD">CAMPOS</td>
                            <td class="divTblListTD">NICOLE ANDREA</td>
                            <td class="divTblListTD">G</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">CAMPOS</td>
                            <td class="divTblListTD">DENISE TANYA</td>
                            <td class="divTblListTD">G</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">FRILLES</td>
                            <td class="divTblListTD">RAMYL FELIX</td>
                            <td class="divTblListTD">C.</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>

                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">REGUDO</td>
                            <td class="divTblListTD">JOELLIANE FAITH</td>
                            <td class="divTblListTD">C.</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">TAGLE</td>
                            <td class="divTblListTD">JASMINE ANN</td>
                            <td class="divTblListTD">S</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">BAUTISTA</td>
                            <td class="divTblListTD">JOHN LUIS</td>
                            <td class="divTblListTD">G</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">DIZON</td>
                            <td class="divTblListTD">ANNA MIKAYLA</td>
                            <td class="divTblListTD">DG</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">LEE</td>
                            <td class="divTblListTD">JAEHYUNG</td>
                            <td class="divTblListTD"></td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">522</td>
                            <td class="divTblListTD">ESPALLARDO</td>
                            <td class="divTblListTD">ARIANNE HAILEIGH</td>
                            <td class="divTblListTD">C</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">JUNIO</td>
                            <td class="divTblListTD">CURT JOSEPH</td>
                            <td class="divTblListTD">J</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">GONDA</td>
                            <td class="divTblListTD">SEBASTIAN</td>
                            <td class="divTblListTD">DG</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>

                        </tr>
                        <tr role="row" class="odd">
                            <td class="divTblListTD" width="100" align="right">525</td>
                            <td class="divTblListTD">FAJARDO</td>
                            <td class="divTblListTD">YZSABELLA</td>
                            <td class="divTblListTD">DJ</td>
                            <td class="divTblListTD">NEW</td>
                            <td>
                                <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                <icon class="glyphicon glyphicon-remove"></icon>
                                <icon class="glyphicon glyphicon-zoom-in"></icon>
                            </td>
                        </tr>


                        </tbody>
                        <tfoot>
                        </tfoot>
                        </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button previous disabled" id="example1_previous">
                                            <a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a>
                                        </li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a>
                                        </li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                        <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                        <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div><!-- /.box-body -->
                        </div>


                        </div>


                        </div>

                            <!--registration-->
                        <div class="chart tab-pane " id="registration" style="position: relative; height: 100%;">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box bg-aqua">
                                        <span class="info-box-icon"><i class="fa fa-user-plus"></i><a href="admissions_add_student.php"></a></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Add New Student</span>
                                        <span class="info-box-number">
                                            <a href="studentrecord_studinfo.php">Go</a>
                                        </span>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                        </div><!-- /.info-box-content -->
                                    </div><!-- /.info-box -->
                                </div><!-- /.col -->



                            </div>

                            <div class="row">
                            <div class="row container-fluid">


                            <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Student List</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length" id="example1_length">
                                        <label>Show <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div id="example1_filter" class="dataTables_filter">
                                        <label>Search:
                                            <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row"><div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 169px;">Applicant No.</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 215px;">Last Name</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 197px;">First Name</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 145px;">M.I</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Status</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 107px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--<tr role="row" class="odd">
                                <td class="sorting_1">Gecko</td>
                                <td>Firefox 1.0</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td>1.7</td>
                                <td>A</td>
                                <td>A</td>
                            </tr>-->

                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">VERDAN</td>
                                <td class="divTblListTD">MARIA CARYL FAYE</td>
                                <td class="divTblListTD">J. </td>
                                <td class="divTblListTD">NEW</td>
                                <td>

                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">DEL CASTILLO</td>
                                <td class="divTblListTD">JUAN ALFONSO</td>
                                <td class="divTblListTD">Z.</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">ALCANTARA</td>
                                <td class="divTblListTD">JULLIAN IRAH</td>
                                <td class="divTblListTD">M.</td>
                                <td class="divTblListTD">NEW</td>
                                <td>

                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">RENTUTAR</td>
                                <td class="divTblListTD">MATTHEW ANGELO</td>
                                <td class="divTblListTD">T</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">534</td>
                                <td class="divTblListTD">FLORITA</td>
                                <td class="divTblListTD">NICOLE ANGELA</td>
                                <td class="divTblListTD">I.</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">LUCINARIO</td>
                                <td class="divTblListTD">ALEKSEI YOHANN</td>
                                <td class="divTblListTD">T</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">PASIA</td>
                                <td class="divTblListTD">EISEN CASSANDRA</td>
                                <td class="divTblListTD">M</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">SINGH</td>
                                <td class="divTblListTD">GURPAWANDEEP</td>
                                <td class="divTblListTD"></td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">530</td>
                                <td class="divTblListTD">CAMPOS</td>
                                <td class="divTblListTD">NICOLE ANDREA</td>
                                <td class="divTblListTD">G</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">CAMPOS</td>
                                <td class="divTblListTD">DENISE TANYA</td>
                                <td class="divTblListTD">G</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">FRILLES</td>
                                <td class="divTblListTD">RAMYL FELIX</td>
                                <td class="divTblListTD">C.</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>

                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">REGUDO</td>
                                <td class="divTblListTD">JOELLIANE FAITH</td>
                                <td class="divTblListTD">C.</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">TAGLE</td>
                                <td class="divTblListTD">JASMINE ANN</td>
                                <td class="divTblListTD">S</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">BAUTISTA</td>
                                <td class="divTblListTD">JOHN LUIS</td>
                                <td class="divTblListTD">G</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">DIZON</td>
                                <td class="divTblListTD">ANNA MIKAYLA</td>
                                <td class="divTblListTD">DG</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">LEE</td>
                                <td class="divTblListTD">JAEHYUNG</td>
                                <td class="divTblListTD"></td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">522</td>
                                <td class="divTblListTD">ESPALLARDO</td>
                                <td class="divTblListTD">ARIANNE HAILEIGH</td>
                                <td class="divTblListTD">C</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">JUNIO</td>
                                <td class="divTblListTD">CURT JOSEPH</td>
                                <td class="divTblListTD">J</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">GONDA</td>
                                <td class="divTblListTD">SEBASTIAN</td>
                                <td class="divTblListTD">DG</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>

                            </tr>
                            <tr role="row" class="odd">
                                <td class="divTblListTD" width="100" align="right">525</td>
                                <td class="divTblListTD">FAJARDO</td>
                                <td class="divTblListTD">YZSABELLA</td>
                                <td class="divTblListTD">DJ</td>
                                <td class="divTblListTD">NEW</td>
                                <td>
                                    <a href="edit_registration_studinfo.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                                    <icon class="glyphicon glyphicon-remove"></icon>
                                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                                </td>
                            </tr>


                            </tbody>
                            <tfoot>
                            </tfoot>
                            </table>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                        <ul class="pagination">
                                            <li class="paginate_button previous disabled" id="example1_previous">
                                                <a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a>
                                            </li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a>
                                            </li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                            <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div><!-- /.box-body -->
                            </div>

                            </div>

                            </div>


                        </div>


                                <!--enrollment-->


                        <div class="chart tab-pane " id="enrollment" style="position: relative; height: 100%;">

                            <div class="nav-tabs-custom">

                                <!-- Tabs within a box -->
                                <ul class="nav nav-tabs pull-left">
                                    <li class="active"><a href="#enrollment-inner" data-toggle="tab">Enrollment</a></li>
                                    <li class=""><a href="#assessment" data-toggle="tab">Assesment</a></li>
                                </ul>
                                <div class="tab-content no-padding">

                                    <div class="chart tab-pane active" id="enrollment-inner" style="position: relative; height: 100%;">
                                        <div class="row">
                                            <div class="">
                                                <div class="container-fluid">

                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <div class="row">
                                                                <div class="col-xs-3"><h3 class="box-title">Enrollment Permit Form</h3></div>

                                                            </div>

                                                        </div>

                                                        <div class="box-body">

                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <label>Date</label>
                                                                    <input type="date" class="form-control" placeholder="date">
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <label for="">Last Name</label>
                                                                    <input type="text" class="form-control" placeholder="lastname">
                                                                </div>
                                                                <div class="col-xs-4">
                                                                    <label for="">First Name</label>
                                                                    <input type="text" class="form-control" placeholder="firstname">
                                                                </div>
                                                                <div class="col-xs-5">
                                                                    <label for="">Middle Name</label>
                                                                    <input type="text" class="form-control" placeholder="middlename">
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <label for="">School Year</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">2014-2015</option>
                                                                        <option value="">2015-2016</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-xs-6">
                                                                    <label for="">Previos Level</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">2014-2015</option>
                                                                        <option value="">2015-2016</option>
                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <label for="">Previous Section</label>
                                                                    <input type="text" class="form-control" placeholder="previos section">
                                                                </div>

                                                                <div class="col-xs-6">
                                                                    <label for="">Present Level</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">2014-2015</option>
                                                                        <option value="">2015-2016</option>
                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <p></p>

                                                            <div class="row" >
                                                                <div class="col-xs-3">
                                                                    <label>
                                                                        Status
                                                                    </label>
                                                                </div>

                                                            </div>

                                                            <div class="row">

                                                                <div class="col-xs-3">
                                                                    <label>
                                                                        Old<input type="checkbox">
                                                                    </label>

                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label>
                                                                        New<input type="checkbox">
                                                                    </label>
                                                                </div>


                                                            </div>

                                                            <div class="col-xs-6">
                                                                <label for="">Permitted issue by:</label>
                                                                <input class="form-control" type="text" placeholder="Issued by"/>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <label for="">Permitted issued date</label>
                                                                    <input class="form-control" type="date" placeholder="date of inquiry"/>
                                                                </div>



                                                            </div>
                                                            <p></p>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <label for="">Mode of payment</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">mode of payment</option>
                                                                        <option value="">mode of payment</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label for="">Proctor</label>
                                                                    <textarea name="" id="" cols="30" rows="5"></textarea>

                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label for="">No. of Children Presently Enrolled in DPS</label>
                                                                    <input class="form-control" type="text" placeholder=No./>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <label for="">Tel/Mobile No.</label>
                                                                    <input class="form-control" type="number" placeholder="number"/>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <p></p>
                                                                <div class="col-xs-3">
                                                                    <button class="btn btn-primary">Save</button>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="chart tab-pane " id="assessment" style="position: relative; height: 100%;">

                                            <div class="nav-tabs-custom">

                                                <!-- Tabs within a box -->
                                                <ul class="nav nav-tabs pull-left">
                                                    <li class="active"><a href="#assessment-inner" data-toggle="tab">Student With Assesment</a></li>
                                                    <li class=""><a href="#assessment-inner2" data-toggle="tab">Student Without Assesment</a></li>
                                                </ul>
                                                <div class="tab-content no-padding">


                                                        <div class="chart tab-pane active" id="assessment-inner" style="position: relative; height: 100%;">

                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <label for="">SY</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">2014-2015</option>
                                                                        <option value="">2015-2016</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="chart tab-pane " id="assessment-inner2" style="position: relative; height: 100%;">

                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <label for="">SY</label>
                                                                    <select class="form-control" name="" id="">
                                                                        <option value="">2014-2015</option>
                                                                        <option value="">2015-2016</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>


                                                </div>
                                            </div>



                                    </div>

                                    </div>

                            </div>

                        </div>


                        </div><!--end of the tab content-->
                        </div>


                    </div>

                </div><!-- /.row -->
            </div>
            <!-- Main row -->

        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    <?php include_once('footer.php'); ?>

</body>
</html>

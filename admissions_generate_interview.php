<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HEDCen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include_once('header.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                MENU
                <small>Admissions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Registrar</a></li>
                <li class="active">Admissions</li>
                <li class="active">Generate Interview</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Generate Shedule of Interviews</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">

                                <div class="form-group ">
                                    <label for="applicantNumber">From:</label>
                                    <input type="date" class="form-control" id="applicantNumber" placeholder="Enter applicant number">
                                </div>

                                <div class="form-group">
                                    <label for="firstName">To:</label>
                                    <input type="date" class="form-control" id="firstName" placeholder="First Name">
                                </div>


                            </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Generate</button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
            <!--this is for table row-->

            <div class="row container-fluid">

            <!-- row for the table-->
            <div class="row container-fluid">


            <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Generated Interviews</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row">
                <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                        <label>Show <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries
                        </label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div id="example1_filter" class="dataTables_filter">
                        <label>Search:
                            <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row"><div class="col-sm-12">
            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 169px;">Date</th>
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 169px;">Interviews</th>
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 169px;">Actions</th>

            </tr>
            </thead>
            <tbody>
            <!--<tr role="row" class="odd">
                <td class="sorting_1">Gecko</td>
                <td>Firefox 1.0</td>
                <td>Win 98+ / OSX.2+</td>
                <td>1.7</td>
                <td>A</td>
                <td>A</td>
            </tr>-->

            <tr role="row" class="odd">
                <td class="divTblListTD" width="100" align="right"><?php echo date('m/d/y'); ?></td>
                <td class="divTblListTD" width="100" align="right">Student 1</td>

                <td>
                    <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                    <icon class="glyphicon glyphicon-remove"></icon>
                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                </td>
            </tr>

            <tr role="row" class="odd">
                <td class="divTblListTD" width="100" align="right"><?php echo date('m/d/y'); ?></td>
                <td class="divTblListTD" width="100" align="right">Student 2</td>

                <td>
                    <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                    <icon class="glyphicon glyphicon-remove"></icon>
                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                </td>
            </tr>

            <tr role="row" class="odd">
                <td class="divTblListTD" width="100" align="right"><?php echo date('m/d/y'); ?></td>
                <td class="divTblListTD" width="100" align="right">Student 3</td>

                <td>
                    <a href="admissions_add_student.php"><icon class="glyphicon glyphicon-pencil"></icon></a>
                    <icon class="glyphicon glyphicon-remove"></icon>
                    <icon class="glyphicon glyphicon-zoom-in"></icon>
                </td>
            </tr>

            </tbody>
            <tfoot>
            </tfoot>
            </table>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                        <ul class="pagination">
                            <li class="paginate_button previous disabled" id="example1_previous">
                                <a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a>
                            </li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a>
                            </li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                            <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                            <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
            </div><!-- /.box-body -->
            </div>


            </div>


            </div>


            <div class="chart tab-pane active" id="registration" style="position: relative; height: 100%;">
                <div class="row">
                    hello world
                </div>
            </div>


    </div><!--end of the tab content-->
</div>


</div>


</div>


        </section>

    </div><!-- /.content-wrapper -->
    <?php include_once('footer.php'); ?>

</body>
</html>

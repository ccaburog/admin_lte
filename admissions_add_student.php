<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HEDCen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include_once('header.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                MENU
                <small>Registrar Modules</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Registrar Modules </a></li>
                <li class="active">Inquiry</li>
                <li class="active">Add new student</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="container-fluid">
                <div class="row">

                    <div class="box box-primary">
                        <div class="box-header with-border bg-green">
                            <div class="row">
                                <div class="col-xs-3"><h3 class="box-title">Student Applicant No.</h3></div>
                                <div class="col-xs-3"><h3 class="box-title">New</h3></div>
                                <div class="col-xs-3">
                                    <h3 class="box-title">Last grade/Year Lvl Attend</h3>
                                    <select class="form-control" name="" id="">
                                        <option value="">Select student year level</option>
                                    </select>
                                </div>
                                <div class="col-xs-3"><h3 class="box-title">Student Number</h3></div>
                            </div>

                        </div>

                        <div class="box-body">

                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Date</label>
                                    <input type="date" class="form-control" placeholder="date">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="">Last Name</label>
                                    <input type="text" class="form-control" placeholder="lastname">
                                </div>
                                <div class="col-xs-4">
                                    <label for="">First Name</label>
                                    <input type="text" class="form-control" placeholder="firstname">
                                </div>
                                <div class="col-xs-5">
                                    <label for="">Middle Name</label>
                                    <input type="text" class="form-control" placeholder="middlename">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="">Date of Birth</label>
                                    <input type="date" class="form-control" placeholder="date of birth">
                                </div>

                                <div class="col-xs-6">
                                    <label for="">Age</label>
                                    <input type="text" class="form-control" placeholder="age">
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="">Parent's Name</label>
                                    <input type="text" class="form-control" placeholder="parent's name">
                                </div>

                                <div class="col-xs-6">
                                    <label for="">Contact Number</label>
                                    <input type="number" class="form-control" placeholder="contact number">
                                </div>

                            </div>
                            
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>
                                        Preschool<input type="checkbox">
                                    </label>

                                </div>
                                <div class="col-xs-3">
                                    <label>
                                        Gradeschool<input type="checkbox">
                                    </label>
                                </div>

                                <div class="col-xs-3">

                                    <label>
                                        Highschool<input type="checkbox">
                                    </label>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="">Encoded by:</label>
                                    <select name="" id="" class="form-control">
                                        <option value="">Teacher 1</option>
                                        <option value="">Teacher 2</option>
                                        <option value="">Teacher 3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <label for="">Date of Inquiry</label>
                                    <input class="form-control" type="date" placeholder="date of inquiry"/>
                                </div>

                                <div class="col-xs-6">
                                    <label for="">Schedul of assesment</label>
                                    <input class="form-control" type="text" placeholder="schedule of assesment"/>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="">Level</label>
                                    <input class="form-control" type="text" placeholder="level"/>
                                </div>
                                <div class="col-xs-3">
                                    <label for="">Proctor</label>
                                    <input class="form-control" type="text" placeholder="proctor"/>

                                </div>
                                <div class="col-xs-3">
                                    <label for="">Schedule of interview</label>
                                    <input class="form-control" type="text" placeholder="shedule of interview"/>
                                </div>
                                <div class="col-xs-3">
                                    <label for="">Remarks</label>
                                    <input class="form-control" type="text" placeholder="remarks"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="">OR Number</label>
                                    <input class="form-control" type="number" placeholder="OR NO."/>
                                </div>
                            </div>

                            <div class="row">
                                <p></p>
                                <div class="col-xs-3">
                                    <button class="btn btn-primary">Save</button>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    <?php include_once('footer.php'); ?>

</body>
</html>
